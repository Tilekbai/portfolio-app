Склонируйте репозиторий из Gitlab на свой локальный компьютер с помощью команды:

git clone https://gitlab.com/Tilekbai/portfolio-app.gitlab

Перейдите в корневой каталог проекта.

Создайте виртуальное окружение, выполнив команду:

python3 -m venv venv

Активируйте виртуальное окружение, выполнив команду:

source venv/bin/activate

Установите необходимые зависимости, выполнив команду:

pip install -r requirements.txt

Прокатите миграции, выполнив команду:

python manage.py migrate

Запустите сервер разработки, выполнив команду:

python manage.py runserver

Создайте суперюзера:

python mamage.py createsuperuser

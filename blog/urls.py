from django.urls import path
from . import views
from .views import (
    BlogListView,
    BlogCategory,
    PostDetailView,
)

urlpatterns = [
    path("", BlogListView.as_view(), name="blog_index"),
    path("<int:pk>/", PostDetailView.as_view(), name="blog_detail"),
    path("<category>/", BlogCategory.as_view(), name="blog_category"),
]
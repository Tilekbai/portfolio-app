from django.http import HttpResponse
from django.shortcuts import render
from django.views import View
from django.views.generic import (
    ListView,
    TemplateView,
)
from blog.models import (
    Post,
    Comment,
    Category,
)
from .forms import CommentForm


class BlogListView(ListView):
    queryset = Post.objects.all()
    context_object_name = 'posts'
    template_name = 'blog_index.html'

class BlogCategory(TemplateView):
    template_name = 'blog_category.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_pk = Category.objects.filter(name=kwargs['category']).values('pk')[0]['pk']
        context['posts'] = Post.objects.filter(categories=category_pk)
        return context

class PostDetailView(View):
    form_class = CommentForm
    initial = {"key": "value"}
    template_name = "blog_detail.html"
    def get(self, request, *args, **kwargs):
        post = Post.objects.get(pk=kwargs['pk'])
        comments = Comment.objects.filter(post=post)
        form = self.form_class(initial=self.initial)
        context = {
            "post": post,
            "comments": comments,
            "form": form,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        post = Post.objects.get(pk=kwargs['pk'])
        comments = Comment.objects.filter(post=post)
        form = self.form_class(request.POST)
        if form.is_valid():
            comment = Comment(
                author=form.cleaned_data["author"],
                body=form.cleaned_data["body"],
                post=post
            )
            comment.save()
            context = {
                "post": post,
                "comments": comments,
                "form": form,
            }
            return render(request, self.template_name, context)

def see_request(request):
    text = f"""
        Some attributes of the HttpRequest object:

        scheme: {request.scheme}
        path:   {request.path}
        method: {request.method}
        GET:    {request.GET}
        user:   {request.user}
    """

    return HttpResponse(text, content_type="text/plain")

def user_info(request):
    text = f"""
        Selected HttpRequest.user attributes:

        username:     {request.user.username}
        is_anonymous: {request.user.is_anonymous}
        is_staff:     {request.user.is_staff}
        is_superuser: {request.user.is_superuser}
        is_active:    {request.user.is_active}
    """

    return HttpResponse(text, content_type="text/plain")

from django.contrib.auth.decorators import login_required

@login_required
def private_place(request):
    return HttpResponse("Shhh, members only!", content_type="text/plain")

from django.contrib.auth.decorators import user_passes_test

@user_passes_test(lambda user: user.is_staff)
def staff_place(request):
    return HttpResponse("Employees must wash hands", content_type="text/plain")


from django.contrib import messages

@login_required
def add_messages(request):
    username = request.user.username
    messages.add_message(request, messages.INFO, f"Hello {username}")
    messages.add_message(request, messages.WARNING, "DANGER WILL ROBINSON")

    return HttpResponse("Messages added", content_type="text/plain")
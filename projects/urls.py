from django.urls import path
from . import views
from .views import ProjectIndexView, ProjectDetailView

urlpatterns = [
    path("", ProjectIndexView.as_view(), name="project_index"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="project_detail"),
]
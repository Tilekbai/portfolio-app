from django.views.generic import (
    TemplateView,
    DetailView, ListView,
)
from projects.models import Project


class ProjectIndexView(ListView):
    queryset = Project.objects.all()
    context_object_name = 'projects'
    template_name = 'project_index.html'


class ProjectDetailView(DetailView):
    model = Project
    template_name = 'project_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
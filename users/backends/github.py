from social_core.backends.github import GithubOAuth2

class GithubBackend(GithubOAuth2):
    name = 'github'

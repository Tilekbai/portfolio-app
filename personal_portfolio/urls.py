from django.contrib import admin
from django.urls import (
    path,
    include,
    re_path,
)
from blog.views import (
    see_request,
    user_info,
    private_place,
    staff_place,
    add_messages,
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls")),
    path("blog/", include("blog.urls")),
    re_path(r"^", include("users.urls")),
    path("see_request/", see_request),
    path("user_info/", user_info),
    path("private_place/", private_place),
    path("staff_place/", staff_place),
    path("add_messages/", add_messages),
]